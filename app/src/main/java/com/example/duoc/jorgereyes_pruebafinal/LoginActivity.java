package com.example.duoc.jorgereyes_pruebafinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener  {

    private EditText etNombre,etPass;
    private Button btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etNombre = (EditText) findViewById(R.id.ed_nombre);
        etPass = (EditText) findViewById(R.id.ed_contraseña);

        btnLogin =(Button) findViewById(R.id.bt_login);

        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.bt_login){
            //Ingresar();
            Ingresar();
        }
    }

    private void Ingresar() {
            if (etNombre.getText().toString().equals("admin")&&etPass.getText().toString().equals("admin")){
                Toast.makeText(this, "Usuario Valido", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(LoginActivity.this,PerfilActivity.class);
                startActivity(intent);
            }else {
                Toast.makeText(this, "Usuario Invalido Dummy", Toast.LENGTH_LONG).show();
        }
    }
}

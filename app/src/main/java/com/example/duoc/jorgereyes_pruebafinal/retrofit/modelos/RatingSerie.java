package com.example.duoc.jorgereyes_pruebafinal.retrofit.modelos;

import com.google.gson.annotations.SerializedName;

public class RatingSerie {

    @SerializedName("percentage")
    private int percentage;
    @SerializedName("watching")
    private int watching;
    @SerializedName("votes")
    private int votes;
    @SerializedName("loved")
    private int loved;
    @SerializedName("hated")
    private int hated;

    public RatingSerie(int percentage, int watching, int votes, int loved, int hated) {
        this.percentage = percentage;
        this.watching = watching;
        this.votes = votes;
        this.loved = loved;
        this.hated = hated;
    }

    public RatingSerie() {
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public int getWatching() {
        return watching;
    }

    public void setWatching(int watching) {
        this.watching = watching;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int getLoved() {
        return loved;
    }

    public void setLoved(int loved) {
        this.loved = loved;
    }

    public int getHated() {
        return hated;
    }

    public void setHated(int hated) {
        this.hated = hated;
    }
}
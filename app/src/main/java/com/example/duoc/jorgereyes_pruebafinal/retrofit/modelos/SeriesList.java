package com.example.duoc.jorgereyes_pruebafinal.retrofit.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hardroidlabs on 01-07-17.
 */

public class SeriesList {
    @SerializedName("items")
    List<Series> list;

    public SeriesList(List<Series> list) {
        this.list = list;
    }

    public List<Series> getList() {
        return list;
    }

    public void setList(List<Series> list) {
        this.list = list;
    }
}

package com.example.duoc.jorgereyes_pruebafinal.retrofit.modelos;

import com.google.gson.annotations.SerializedName;

public class ImagesSerie {
    /*
    private String poster;
    private String fanart;*/
    @SerializedName("banner")
    private String banner;

    public ImagesSerie() {
    }
/*
    public ImagesSerie(String poster, String fanart, String banner) {
        this.poster = poster;
        this.fanart = fanart;
        this.banner = banner;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getFanart() {
        return fanart;
    }

    public void setFanart(String fanart) {
        this.fanart = fanart;
    }*/

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
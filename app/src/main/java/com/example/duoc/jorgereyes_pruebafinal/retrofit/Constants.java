package com.example.duoc.jorgereyes_pruebafinal.retrofit;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public class Constants {
    static public final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    static public final int API_CONNECTION_TIMEOUT = 10;
    public interface APIConstants {
        String ANIME_LIST = "bins/1f1xe7";
    }
}

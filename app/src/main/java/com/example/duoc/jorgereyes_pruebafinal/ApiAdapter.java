package com.example.duoc.jorgereyes_pruebafinal;

/**
 * Created by DUOC on 01-07-2017.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duoc.jorgereyes_pruebafinal.retrofit.modelos.Series;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ApiAdapter extends RecyclerView.Adapter<ApiAdapter.ApiViewHolder>{

    private ArrayList<Series> dataSource;
    private Context context;

    public ApiAdapter(Context context) {
        this.context= context;
        dataSource = new ArrayList<>();
    }

    @Override
    public ApiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_anime,parent,false);
        return new ApiViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ApiViewHolder holder, int position) {
        Series s = dataSource.get(position);
        holder.tv_nombre.setText(s.getTitle());
        holder.tv_año.setText(s.getYear());
        /*String[] array=s.getGenres();
        String aux_genero="";
        for (int i = 0; i <s.getGenres().length ; i++) {
            aux_genero=aux_genero+array[i].getClass().;
        }
        holder.tv_genero.setText(aux_genero);*/
        holder.tv_num_temporadas.setText(""+s.getNumSeasons());
        holder.tv_rating.setText(""+s.getRatingSerie().getPercentage()+"%");
        Picasso.with(context).load(s.getPoster().getBanner()).into(holder.iv_foto);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public void adiccionarLista(List<Series> series) {
        dataSource.addAll(series);
        notifyDataSetChanged();
    }

    public class ApiViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_nombre,tv_año,tv_genero,tv_rating,tv_num_temporadas;
        private ImageView iv_foto;

        public ApiViewHolder(View itemView) {
            super(itemView);

            tv_nombre = (TextView) itemView.findViewById(R.id.tv_nombre_anime);
            tv_año = (TextView) itemView.findViewById(R.id.tv_año_anime);
            tv_genero = (TextView) itemView.findViewById(R.id.tv_genero_anime);
            tv_rating = (TextView) itemView.findViewById(R.id.tv_rating_anime);
            tv_num_temporadas = (TextView) itemView.findViewById(R.id.tv_num_temporadas);
            iv_foto = (ImageView) itemView.findViewById(R.id.iv_foto);
        }
    }
}

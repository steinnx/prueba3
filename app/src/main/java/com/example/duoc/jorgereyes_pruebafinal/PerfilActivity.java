package com.example.duoc.jorgereyes_pruebafinal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.example.duoc.jorgereyes_pruebafinal.retrofit.ApiEndPointInterface;
import com.example.duoc.jorgereyes_pruebafinal.retrofit.modelos.Series;
import com.example.duoc.jorgereyes_pruebafinal.retrofit.modelos.SeriesList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.security.AccessController.getContext;

public class PerfilActivity extends AppCompatActivity {

    private Retrofit retrofit;
    //reciclerview
    private RecyclerView rv_listar;
    private ApiAdapter apiAdapter;

    private Toolbar toolbar;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        toolbar = (Toolbar) findViewById(R.id.id_toolbar);
        toolbar.setTitle("Jorge Reyes");
        setSupportActionBar(toolbar);
        context=this;
        rv_listar = (RecyclerView) findViewById(R.id.rv_listar);
        apiAdapter = new ApiAdapter(this);
        rv_listar.setAdapter(apiAdapter);
        rv_listar.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(this,1);
        rv_listar.setLayoutManager(layoutManager);



        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        obtenerDatos();

        /*rv_listar.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Intent i = new Intent(context, DetalleActivity.class);
                        i.putExtra("posicion",position);
                        getContext().startActivity(i);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );*/
    }

    private void obtenerDatos() {
        ApiEndPointInterface anInterface = retrofit.create(ApiEndPointInterface.class);
        Call<SeriesList> seriesListCall = anInterface.getListAnime();
        seriesListCall.enqueue(new Callback<SeriesList>() {
            @Override
            public void onResponse(Call<SeriesList> call, Response<SeriesList> response) {
                if (response.isSuccessful()){
                    SeriesList seriesList = response.body();
                    List<Series> series = seriesList.getList();
                    apiAdapter.adiccionarLista(series);
                }else {
                    Log.e("ERRORRRRR!!!!!!!!!!!!!!","onResponeseeeeeeeeeeee: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SeriesList> call, Throwable t) {
                Log.e("ERRORRRRR!!!!!!!!!!!!!!","onFailureeeeeeeeeee: "+t.getMessage());
            }
        });

    }
}
